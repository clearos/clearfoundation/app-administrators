<?php

$lang['administrators_app_description'] = 'Với các ứng dụng quản trị viên, bạn có thể cấp quyền truy cập vào các ứng dụng cụ thể cho các nhóm người dùng trên hệ thống.';
$lang['administrators_app_name'] = 'Quản trị';
